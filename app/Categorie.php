<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    //
    protected $fillable = ['nom'];

        public function competences() {
            return $this->hasMany(Competence::class,'clef_categorie','id');
        }
}
