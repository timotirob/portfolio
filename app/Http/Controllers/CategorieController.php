<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Categorie::all()->toArray());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       $categorie = Categorie::create($request->only('nom'));

                   return response()->json([
                       'status' => (bool) $categorie,
                       'message'=> $categorie ? 'Categorie OK' : 'Erreur lors de la création'
                   ]);     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function show(Categorie $categorie)
    {
        //
        return response()->json($categorie);

    }

    public function competences(Categorie $categorie)
            {
                return response()->json($categorie->competences()->orderBy('ordre')->get());
            }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function edit(Categorie $categorie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categorie $categorie)
    {
        //
        $status = $categorie->update($request->only('nom'));

            return response()->json([
                'status' => $status,
                'message' => $status ? 'Catégorie mise à jour!' : 'Erreur de MAJ de la catégorie'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
     public function destroy(Categorie $categorie)
     {
       $status  = $categorie->delete();

                   return response()->json([
                       'status' => $status,
                       'message' => $status ? 'Categorie delete' : 'Erreur lors de la suppression'
                   ]);     }
}
