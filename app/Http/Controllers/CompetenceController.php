<?php

namespace App\Http\Controllers;

use App\Competence;
use Illuminate\Http\Request;

class CompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Competence::all()->toArray());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $competence = Competence::create([
            'nom' => $request->nom,
            'clef_categorie' => $request->clef_categorie,
            'clef_user' => $request->clef_user,
            'ordre' => $request->ordre
        ]);

        return response()->json([
                'status' => (bool) $competence,
                'data'   => $competence,
                'message' => $competence ? 'OK creation' : 'Erreur creation competence'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function show(Competence $competence)
    {
        //
        return response()->json($competence);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function edit(Competence $competence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Competence $competence)
    {
        //
        $status = $competence->update(
                $request->only(['nom', 'clef_categorie', 'clef_user', 'ordre'])
            );

            return response()->json([
                'status' => $status,
                'message' => $status ? 'Tache MAJ' : 'Erreur MAJ'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Competence  $competence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Competence $competence)
    {
        //
        $status = $competence->delete();

        return response()->json([
                'status' => $status,
                'message' => $status ? 'Tache supprimee' : 'Erreur suppression'
            ]);
    }
}
