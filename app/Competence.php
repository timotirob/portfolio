<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    //
    protected $fillable = ['nom', 'clef_categorie', 'clef_user', 'ordre'];

        public function categorie() {
            return $this->hasOne(Categorie::class,'clef_categorie');
        }

        public function user() {
            return $this->belongsTo(User::class,'clef_user');
        }

}
