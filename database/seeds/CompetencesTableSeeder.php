<?php

use Illuminate\Database\Seeder;
use App\Competence;


class CompetencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Competence::create(['nom' => 'Test competence',
                            'clef_categorie' => 1,
                            'clef_user' => 1,
                            'ordre' => 1
      ]);
    }
}
