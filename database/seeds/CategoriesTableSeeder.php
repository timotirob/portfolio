<?php

use App\Categorie;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = ['Développement', 'Data', 'Agilite'];

            foreach ($categories as $categorie) {
                Categorie::create(['nom' => $categorie]);
            }
    }
}
