<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APITest extends TestCase
{

     public function testCreationUtilisateur()
         {
             $response = $this->json('POST', '/api/register', [
                 'nom' => 'Timoto test unitaire',
                 'email' => str_random(10) . '@demo.com',
                 'password' => '12345',
             ]);

             $response->assertStatus(200)->assertJsonStructure([
                 'success' => ['token', 'nom']
             ]);
         }

    public function testLoginUser()
    {
        $response = $this->json('POST', '/api/login', [
            'email' => 'timoto.btssio@gmail.com',
            'password' => 'degoute2018'
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            'success' => ['token']
        ]);
    }

    public function testRecuperationCategorie()
    {
        $user = \App\User::find(1);

        $response = $this->actingAs($user, 'api')
            ->json('GET', '/api/categorie')
            ->assertStatus(200)->assertJsonStructure([
                '*' => [
                    'id',
                    'nom',
                    'created_at',
                    'updated_at'
                ]
            ]
        );
    }

    public function testCreationCategorie()
    {
        $this->withoutMiddleware();

        $response = $this->json('POST', '/api/categorie', [
            'nom' => str_random(10),
        ]);

        $response->assertStatus(200)->assertJson([
            'status' => true,
            'message' => 'Categorie OK'
        ]);
    }

    public function testSuppressionCategorie()
        {
            $user = \App\User::find(1);

            $categorie = \App\Categorie::create(['nom' => 'Categorie à supprimer']);

            $response = $this->actingAs($user, 'api')
                ->json('DELETE', "/api/categorie/{$categorie->id}")
                ->assertStatus(200)->assertJson([
                    'status' => true,
                    'message' => 'Categorie delete'
                ]);
        }

        public function testCreationCompetence()
        {
            $this->withoutMiddleware();

            $response = $this->json('POST', '/api/competence', [
                'nom' => str_random(10),
                'clef_categorie' => 1,
                'clef_user' => 1,
                'ordre' => 1
            ]);

            $response->assertStatus(200)->assertJson([
                'status' => true,
                'message' => 'OK creation'
            ]);
        }

}
