import Vue          from 'vue'
    import VueRouter    from 'vue-router'

    Vue.use(VueRouter)

    import App          from './components/App'
    import Dashboard    from './components/Board'
    import Login        from './components/Login'
    import Register     from './components/Register'
    import Bienvenue         from './components/Bienvenue'

    const router = new VueRouter({
        mode: 'history',
        routes: [
            {
                path: '/',
                name: 'bienvenue',
                component: Bienvenue
            },
            {
                path: '/login',
                name: 'login',
                component: Login,
            },
            {
                path: '/register',
                name: 'register',
                component: Register,
            },
            {
                path: '/board',
                name: 'board',
                component: Dashboard,
            },
        ],
    });

    const app = new Vue({
        el: '#app',
        components: { App },
        router,
    });
